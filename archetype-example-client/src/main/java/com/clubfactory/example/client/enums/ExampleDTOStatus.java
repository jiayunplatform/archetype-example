package com.clubfactory.example.client.enums;

/**
 * @author lianghaijun
 * @date 2018/9/27
 */
public enum ExampleDTOStatus {

    SAVED,      //保存
    PUBLISH,    //发布
    DELETED;    //删除

}
