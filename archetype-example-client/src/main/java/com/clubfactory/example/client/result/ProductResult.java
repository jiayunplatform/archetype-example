package com.clubfactory.example.client.result;

import com.clubfactory.boot.toolkit.error.ErrorMessage;
import com.clubfactory.boot.toolkit.result.BaseResult;

/**
 *
 *  定制的Result
 *
 * @author lianghaijun
 * @date 2018-09-19
 */
public class ProductResult extends BaseResult {

    public static ProductResult success() {
        return new ProductResult();
    }

    public static ProductResult failure(ErrorMessage errorMessage) {
        ProductResult result = new ProductResult();
        result.addErrorMessage(errorMessage);
        return result;
    }

}
