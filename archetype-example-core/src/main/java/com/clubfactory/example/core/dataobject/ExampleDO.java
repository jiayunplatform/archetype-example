package com.clubfactory.example.core.dataobject;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by huage on 2018/8/23.
 */
@Data
public class ExampleDO implements Serializable {

    private int id;

    private int userId;

    private int templateId;

    private String orderName;

    private String discount;

    private String state;

    private Date startDate;

    private Date expiryDate;

    private int flag;

    private Date createDate;

    private Date updateDate;

}
