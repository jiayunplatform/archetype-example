package com.clubfactory.example.core.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;


@SuppressWarnings("SpringJavaAutoWiringInspection")
@Configuration
@MapperScan(value = "com.clubfactory.example.core.dao")
@EnableTransactionManagement
@Slf4j
public class MybatisConfig {

    @Autowired
    @Qualifier("couponDataSource")
    private DataSource dataSource;

    @Bean
    public SqlSessionFactory sqlSessionFactories() throws Exception {
        log.info("------------- 重载父类 sqlSessionFactory init-------");
        SqlSessionFactoryBean factory = new SqlSessionFactoryBean();
        factory.setDataSource(dataSource);

        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        factory.setMapperLocations(resolver.getResources("classpath:/mapper/*Mapper.xml"));
        return factory.getObject();
    }
}
