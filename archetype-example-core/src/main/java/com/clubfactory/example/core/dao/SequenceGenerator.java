package com.clubfactory.example.core.dao;

import org.springframework.stereotype.Repository;

import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * 这里是个DEMO, 实际中的序列化ID, 应该由分库分表中间件产生
 *
 * @author lianghaijun
 * @date 2018/9/19
 */
@Repository
public class SequenceGenerator {

    //id产生器, 这里仅仅是个DEMO
    private AtomicLong idGenerator = new AtomicLong();

    public Long generateNextId(String tableName) {
        return idGenerator.incrementAndGet();
    }

}
