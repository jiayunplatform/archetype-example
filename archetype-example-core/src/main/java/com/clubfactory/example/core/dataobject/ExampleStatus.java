package com.clubfactory.example.core.dataobject;

/**
 * @author lianghaijun
 * @date 2018/9/19
 */
public class ExampleStatus {

    public static final int SAVED = 1;
    public static final int PUBLISH = 2;
    public static final int DELETED = -1;

}
