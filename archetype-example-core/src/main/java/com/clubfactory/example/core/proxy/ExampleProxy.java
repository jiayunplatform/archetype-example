package com.clubfactory.example.core.proxy;

import org.springframework.stereotype.Service;

/**
 *
 * Proxy用于对第三方服务的封装
 *
 * Created by huage on 2018/8/6.
 */
@Service
public class ExampleProxy {

}
