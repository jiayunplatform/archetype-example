package com.clubfactory.example.server.handler;

import com.clubfactory.boot.toolkit.error.ErrorMessage;

/**
 * @author lianghaijun
 * @date 2018/9/19
 */
public class ProviderExceptionHandler {

    public static ErrorMessage processException(Exception e) {
        return ErrorMessage.of("S-00-00-00", e.getMessage());
    }

}
