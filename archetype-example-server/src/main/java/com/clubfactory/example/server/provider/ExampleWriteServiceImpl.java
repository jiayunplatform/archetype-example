package com.clubfactory.example.server.provider;

import com.alibaba.dubbo.config.annotation.Service;
import com.clubfactory.boot.toolkit.error.ErrorMessage;
import com.clubfactory.boot.toolkit.result.Result;
import com.clubfactory.example.client.dto.ExampleDTO;
import com.clubfactory.example.client.result.ProductResult;
import com.clubfactory.example.client.service.ExampleWriteService;
import com.clubfactory.example.core.dataobject.ExampleDO;
import com.clubfactory.example.core.service.ExampleService;
import com.clubfactory.example.server.handler.ProviderExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;


/**
 * Service 用于SOA提供对外服务
 * Created by huage on 2018/8/6.
 */
@Service
public class ExampleWriteServiceImpl implements ExampleWriteService {

    @Autowired
    private ExampleService exampleService;

    private BeanCopier beanCopier = BeanCopier.create(ExampleDTO.class, ExampleDO.class, false);

    @Override
    public Result<Long> save(ExampleDTO exampleDTO) {
        try {
            ExampleDO exampleDO = mapping(exampleDTO);
            long newProductId = exampleService.save(exampleDO);
            return Result.of(newProductId);
        } catch(Exception e) {
            ErrorMessage errorMessage = ProviderExceptionHandler.processException(e);
            return Result.of(errorMessage);
        }
    }

    @Override
    public ProductResult publish(ExampleDTO exampleDTO) {
        try {
            ExampleDO exampleDO = mapping(exampleDTO);
            exampleService.publish(exampleDO);
            return ProductResult.success();
        } catch(Exception e) {
            ErrorMessage errorMessage = ProviderExceptionHandler.processException(e);
            return ProductResult.failure(errorMessage);
        }
    }

    private ExampleDO mapping(ExampleDTO exampleDTO) {
        ExampleDO exampleDO = new ExampleDO();
        beanCopier.copy(exampleDTO, exampleDO, null);
//        switch(exampleDTO.getStatus()) {
//            case SAVED:
//                exampleDO.setStatus(ExampleStatus.SAVED);
//                break;
//            case PUBLISH:
//                exampleDO.setStatus(ExampleStatus.PUBLISH);
//                break;
//            case DELETED:
//                exampleDO.setStatus(ExampleStatus.DELETED);
//                break;
//        }
        return exampleDO;
    }

}
