package com.clubfactory.example.server.config;

import com.clubfactory.boot.autoconfigure.datasource.ClubDataSourceFactory;
import com.clubfactory.boot.autoconfigure.datasource.RoutableDataSourceFactory;
import com.clubfactory.boot.autoconfigure.datasource.RoutableStrategy;
import com.clubfactory.boot.autoconfigure.redis.ClubRedisFactory;
import com.clubfactory.boot.config.ClubConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.sql.DataSource;

/**
 * @author lianghaijun
 * @date 2018-11-20
 */
@Configuration
public class ApplicationConfiguration {

    public static final String REDIS_ORDER_NAME = "order";

    @Primary
    @Bean("orderDataSource")
    @ClubConfigurationProperties("club-boot.datasource.order")
    public DataSource primaryDataSource() {
        return ClubDataSourceFactory.create();
    }

    @Bean("odoo")
    @RoutableStrategy(strategy = "parameter", param="zone", useDefault = true)
    @ClubConfigurationProperties("club-boot.datasource.odoo")
    public DataSource idoo() {
        return RoutableDataSourceFactory.create();
    }

    @Bean("couponDataSource")
    @ClubConfigurationProperties("club-boot.datasource.coupon")
    public DataSource secondDataSource() {
        return ClubDataSourceFactory.create();
    }

    @Primary
    @Bean("orderRedisTemplate")
    @ClubConfigurationProperties("club-boot.redis.order")
    public StringRedisTemplate primaryRedis() {
        return ClubRedisFactory.createStringTemplate();
    }

}
