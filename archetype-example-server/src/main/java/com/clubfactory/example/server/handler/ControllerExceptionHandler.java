package com.clubfactory.example.server.handler;

import com.clubfactory.boot.toolkit.error.ErrorMessage;
import com.clubfactory.boot.toolkit.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;

/**
 * controller的异常处理handler
 * Created by huage on 2018/8/23.
 */
@ControllerAdvice
@Slf4j
public class ControllerExceptionHandler {

    public static final String DEFAULT_ERROR_VIEW = "error";

    @ExceptionHandler(value = NoHandlerFoundException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Result<String> handler404(HttpServletRequest req, Exception e) throws Exception {
        Result<String> result = new Result<>();
        result.addErrorMessage(ErrorMessage.of(HttpStatus.NOT_FOUND.toString(), e.getMessage()));
        return result;
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Result<String> handlerError(HttpServletRequest req, Exception e) throws Exception {
        Result<String> result = new Result<>();
        result.addErrorMessage(ErrorMessage.of(HttpStatus.INTERNAL_SERVER_ERROR.toString(), e.getMessage()));
        log.error("server error", e);
        return result;
    }

}
