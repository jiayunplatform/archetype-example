package com.clubfactory.example.server.controller;

import com.ctrip.framework.apollo.Config;
import com.ctrip.framework.apollo.ConfigService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *  apollo配置, 如需生效, 需要引入 club-boot-starter-apollo
 *
 *
 * @author lianghaijun
 * @date 2018-11-18
 */
@Controller
@RequestMapping("/")
public class ConfigController {

    @RequestMapping(value = "/config", produces = "application/json")
    @ResponseBody
    public String show(@RequestParam(value = "ns", defaultValue = "application") String namespace) {
        Config config = ConfigService.getConfig(namespace);

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode obj = mapper.createObjectNode();
        config.getPropertyNames().forEach(n -> {
            obj.put(n, config.getProperty(n, null));
        });
        return obj.toString();
    }

}
